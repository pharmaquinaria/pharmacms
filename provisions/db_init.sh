#!/usr/bin/env bash

pm_database="pharma_wp"

echo "Creating the database '$pm_database'"
sudo mysql -u root -proot < /vagrant/conf/db/pharma_wp.sql

sudo mysql -u root -proot mysql <<< "GRANT ALL ON *.* TO 'root'@'%'; FLUSH PRIVILEGES;"