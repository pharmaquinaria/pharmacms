#!/usr/bin/env bash

apt-get update

apt-get install -y apache2

if ! [ -L /var/www/html ]; then
  rm -rf /var/www/html
  ln -fs /vagrant/projects /var/www/html
fi

debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install -y mysql-server php5-mysql

apt-get install -y php5 libapache2-mod-php5 php5-curl php5-mcrypt php5-dev

pecl install xdebug

#sudo echo 'zend_extension="/usr/lib/php5/20121212/xdebug.so"' >> /etc/php5/apache2/php.ini

a2enmod rewrite

service apache2 restart 
