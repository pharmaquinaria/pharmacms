#!/usr/bin/env bash

echo "Setting vhosts"

sudo echo "127.0.0.1 	local.pharmacms.com" >> /etc/hosts

sudo cp -f /vagrant/conf/vhost/pharmacms.conf /etc/apache2/sites-available/pharmacms.conf

sudo a2ensite pharmacms.conf 

sudo service apache2 restart