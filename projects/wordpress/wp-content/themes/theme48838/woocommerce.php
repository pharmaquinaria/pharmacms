<?php get_header(); ?>

<div class="">
	<div class="container">
		<div class="row">
			<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" >
				<div class="row">
					<div class="span2">
					</div>
					<div class="span8" id="content">
						<?php woocommerce_content(); ?>
					</div>
					<div class="span2">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>