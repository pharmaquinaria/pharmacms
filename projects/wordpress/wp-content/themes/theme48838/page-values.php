<?php get_header(); ?>

<div class="">
	<div class="container">
		<div class="row">
			<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" >
				<div class="row">
					<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" style="text-align:center">
						<?php get_template_part("static/static-title"); ?>
					</div>
				</div>
				<div class="row" style="margin-bottom: 50px;">
					<div class="span4"></div>
					<div class="span2">
						<ul>
  							<li>Cooperación</li>
  							<li>Innovación</li>
  							<li>Seguridad</li>
  							<li>Amabilidad</li>
  							<li>Confianza</li>
  							<li>Atención</li>
  						</ul>
					</div>
					<div class="span2">
						<ul>
  							<li>Motivación</li>
  							<li>Compromiso</li>
  							<li>Competitividad</li>
  							<li>Ética profesional</li>
  							<li>Apertura al cambio</li> 
  						</ul>
					</div>
					<div class="span4"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>