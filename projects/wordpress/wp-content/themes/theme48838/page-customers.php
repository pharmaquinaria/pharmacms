<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" >
			<div class="row">
				<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" style="text-align:center">
					<?php get_template_part("static/static-title"); ?>
				</div>
			</div>
			<div class="row" style="margin-bottom: 50px;">
				<div class="span4">
					<a href="https://www.boehringer-ingelheim.com.mx/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/boehringer.gif">
					</a>
				</div>
				<div class="span4">
					<a href="http://www.sblpharma.com/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/sbl.png">
					</a>
				</div>
				<div class="span4">
					<a href="http://www.grupoypenza.com/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/ypenza.png">
					</a>
				</div>
			</div>
			<div class="row" style="margin-bottom: 50px;">
				<div class="span4">
					<a href="http://oncord.com.mx/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/oncord.png">
					</a>
				</div>
				<div class="span4">
					<a href="http://www.cid-h.com/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/cidh.png">
					</a>
				</div>	
				<div class="span4">
					<a href="http://www.consorciorayca.com/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/rayca.png">
					</a>
				</div>
			</div>
			<div class="row" style="margin-bottom: 50px;">
				<div class="span4">
					<a href="http://www.aerobal.com/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/aerobal.jpg">
					</a>
				</div>
				<div class="span4">
					<a href="http://www.imanes.com.mx/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/magnetika.png">
					</a>
				</div>
				<div class="span4">
					<a href="http://www.dulcerama.com.mx/" target="_blank">
						<img style="height:100px; width:160px;" class="img-responsive" src="http://local.pharmacms.com/wp-content/themes/theme48838/images/customers/dulcerama.jpg">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>