<?php

add_filter( 'woocommerce_product_tabs', 'pm_woo_remove_reviews_tab', 98);

function pm_woo_remove_reviews_tab($tabs) {

 unset($tabs['reviews']);

 return $tabs;
}

add_filter( 'woocommerce_catalog_orderby', 'pm_woo_catalog_orderby'); 

function pm_woo_catalog_orderby($options) {

 return array(
			'menu_order' => __( 'Default', 'woocommerce' ),
			'date'       => __( 'Nuevos', 'woocommerce' ),
			'price'      => __( 'Por precio: bajo-alto', 'woocommerce' ),
			'price-desc' => __( 'Por precio: alto-bajo', 'woocommerce' )
			);
}

add_filter( 'product_type_selector', 'pm_woo_product_type_selector'); 

function pm_woo_product_type_selector($products) {

 unset($products['grouped']);
 unset($products['external']);
 unset($products['variable']);

 return $products;
}

add_filter( 'product_type_options', 'pm_woo_product_type_options'); 

function pm_woo_product_type_options($options) {

 return array();
}

add_filter( 'woocommerce_product_data_tabs', 'pm_woo_product_data_tabs'); 

function pm_woo_product_data_tabs($tabs) {

 unset($tabs['inventory']);
 unset($tabs['linked_product']);
 unset($tabs['attribute']);
 unset($tabs['variations']);
 unset($tabs['advanced']);

 return $tabs;
}

add_filter('woocommerce_is_purchasable', 'pm_woo_is_purchasable');

function pm_woo_is_purchasable($purchasable) {

 return false;
}